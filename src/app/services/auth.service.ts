import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions,Response} from '@angular/http';
import {User} from "../model/model.user";
import 'rxjs/add/operator/map';
import {AppComponent} from "../app.component";
@Injectable()
export class AuthService {
  constructor(public http: Http) { }

  public logIn(user: User){

    let headers = new Headers();
    headers.append('Accept', 'application/json')
    // creating base64 encoded String from user name and password
    var base64Credential: string = btoa( user.username+ ':' + user.password);
    headers.append("Authorization", "Basic " + base64Credential);
    //headers.append("Access-Control-Allow-Origin","http://localhost:4200")
    let options = new RequestOptions();
    options.headers=headers;
    //  options.withCredentials=true;

    return this.http.get("/api/account/login" ,   options)
    // .subscribe(response=>{
    //   console.log(response);
    //   console.log(response.headers.get("JSESSIONID"))
    // })
      .map((response: Response) => {
      // login successful if there's a jwt token in the response
      let user = response.json().principal;// the returned user object is a principal object
      if (user) {
        // store user details  in local storage to keep user logged in between page refreshes
        localStorage.setItem('currentUser', JSON.stringify(user));
      }
    });
  }

  logOut() {
    // remove user from local storage to log user out
    console.log("logout called");

     return this.http.post("/api/logout",{message:"log me out"});
      // .map((response: Response) => {
      //   localStorage.removeItem('currentUser');
      // });

  }

  testLoggedInUser(){

    this.http.get("/api/test/1").subscribe(
      response=>{
        console.log(response);
      }
    )
  }
}
